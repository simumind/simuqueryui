package com.simuwell.dao;

import com.simuwell.domain.User;

public interface UserDao {
	public User selectUserById(Integer userId);

}
