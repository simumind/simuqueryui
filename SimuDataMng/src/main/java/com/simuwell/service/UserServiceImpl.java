package com.simuwell.service;

import javax.annotation.Resource;
import org.springframework.stereotype.Service;

import com.simuwell.dao.UserDao;
import com.simuwell.domain.User;

@Service
public class UserServiceImpl implements UserService{
	@Resource
	private UserDao userDao;

	public User selectUserById(Integer userId) {
		return userDao.selectUserById(userId);
		
	}

}
