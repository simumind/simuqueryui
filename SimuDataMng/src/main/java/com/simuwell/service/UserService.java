package com.simuwell.service;

import com.simuwell.domain.User;

public interface UserService {

	User selectUserById(Integer userId);

}
